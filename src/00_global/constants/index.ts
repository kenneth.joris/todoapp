export const PATHS = {
	HOME: '/',
	TODOS: '/todos',
} as const;

export const FIELDNAMES = {
	// Login
	ID: 'id',
	NAME: 'name',
	EMAIL: 'email',
	PASSWORD: 'password',

	// Todos
	TITLE: 'title',
	DESCRIPTION: 'description',
	ASSIGNEE: 'assignee',
} as const;

export const API_ENDPOINTS = {
	RANDOM_USER: 'https://randomuser.me/api/1.0/?seed=cached&results=1&inc=name,login,picture',
	RANDOM_USERS: 'https://randomuser.me/api/1.0/?seed=cached&results=5&inc=name,login,picture',
} as const;