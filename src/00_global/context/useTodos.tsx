"use client";

import React, { createContext, useContext, useEffect, useState } from 'react';
import { useSessionStorage } from '@/00_global/hooks/useSessionStorage';
import { v4 as uuidv4 } from 'uuid';
import { useUser } from '@/00_global/context/useUser';

type TodosProviderProps = {
	children: React.ReactNode;
};

export type TodoType = {
	id: string;
	title: string | undefined;
	description: string | undefined;
	assignee: string;
	done: boolean;
	editMode: boolean;
};

type TodosContextType = {
	todos: TodoType[];
	addTodo: () => void,
	updateTodo: ({ id, title, description, assignee, done, editMode }: Partial<TodoType> & Pick<TodoType, 'id'>) => void,
	deleteTodo: ({ id }: Pick<TodoType, 'id'>) => void
};

const TodosContext = createContext<TodosContextType>({} as TodosContextType);
const TodosContextProvider = ({ children }: TodosProviderProps) => {
	const { id: userID } = useUser();
	const { sessionStoreValue, setSessionStoreValue } = useSessionStorage('todos', []);
	const [todos, setTodos] = useState<TodoType[]>([]);

	useEffect(() => {
		setTodos((sessionStoreValue as TodoType[]) ?? []);
	}, [sessionStoreValue])

	const addTodo = () => {
		const newTodo = {
			id: uuidv4(),
			title: undefined,
			description: undefined,
			assignee: userID,
			done: false,
			editMode: true,
		};
		setSessionStoreValue([...todos, newTodo]);
		setTodos((prevTodos) => [ ...prevTodos, newTodo ]);
	};

	const updateTodo = ({ id, title, description, assignee, done, editMode }: Partial<TodoType> & Pick<TodoType, 'id'>) => {
		const updatedTodos = todos.map((todo) => {
			if (todo.id !== id) return todo;
			return {
				id: id,
				title: title ?? todo.title,
				description: description ?? todo.description,
				assignee: assignee ?? todo.assignee,
				done: done ?? todo.done,
				editMode: editMode ?? todo.editMode,
			};
		})
		setSessionStoreValue(updatedTodos);
		setTodos(updatedTodos);
	};

	const deleteTodo = ({ id }: Pick<TodoType, 'id'>) => {
		const updatedTodos = todos.filter((todo) => todo.id !== id);
		setSessionStoreValue(updatedTodos);
		setTodos(updatedTodos);
	};

	return (
		<TodosContext.Provider value={{
			todos: todos,
			addTodo: addTodo,
			updateTodo: updateTodo,
			deleteTodo: deleteTodo,
		}}>{ children }</TodosContext.Provider>
	);

};
const useTodos = () => useContext(TodosContext);
export { useTodos, TodosContextProvider };
