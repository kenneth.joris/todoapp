"use client";

import React, { createContext, useContext } from 'react';
import { useSession } from 'next-auth/react';

type UserProviderProps = {
	children: React.ReactNode;
};

type UserContextType = {
	email: string;
	id: string;
	image: string;
	name: string;
};

const UserContext = createContext<UserContextType>({} as UserContextType);
const UserContextProvider = ({ children }: UserProviderProps) => {
	const { data: session } = useSession();

	return (
		<UserContext.Provider value={{
			email: session?.user?.email ?? '',
			id: session?.user?.id ?? '',
			image: session?.user?.image ?? '',
			name: session?.user?.name ?? '',
		}}>{ children }</UserContext.Provider>
	);
};

const useUser = () => useContext(UserContext);
export { useUser, UserContextProvider };