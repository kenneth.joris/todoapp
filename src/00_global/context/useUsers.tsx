"use client";

import React, { createContext, useContext } from 'react';
import { API_ENDPOINTS } from '@/00_global/constants';

type UsersProviderProps = {
	children: React.ReactNode;
};

export type User = {
	name: {
		title: string;
		first: string;
		last: string;
	},
	login: {
		username: string;
		password: string;
		salt: string;
		md5: string;
		sha1: string;
		sha256: string;
	},
	picture: {
		large: string;
		medium: string;
		thumbnail: string;
	}
};

type UsersContextType = {
	users: User[];
};

const getUsers = async (): Promise<User[]> => {
	const resp = await fetch(API_ENDPOINTS.RANDOM_USERS, {
		cache: 'force-cache',
		method: 'GET',
		headers: { "Content-Type": "application/json" },
	});

	if (!resp.ok) {
		throw new Error('Failed to fetch users!');
	}

	const data =  await resp.json();
	const users = data?.results ?? [];
	return users;
}

const UsersContext = createContext<UsersContextType>({} as UsersContextType);
const UsersContextProvider = async ({ children }: UsersProviderProps) => {
	const users = await getUsers();
	return (
		<UsersContext.Provider value={{
			users: users
		}}>{ children }</UsersContext.Provider>
	);
};

const useUsers = () => useContext(UsersContext);
export { useUsers, UsersContextProvider };