import '@testing-library/jest-dom';
import { useFormFieldValidation } from '@/00_global/hooks/useFormFieldValidation';
import { FIELDNAMES } from '@/00_global/constants';
import { render, waitFor } from '@testing-library/react';

const usedField = FIELDNAMES.NAME;

const TestComponent = ({ value }: { value: string | undefined }) => {
	const { isValid, errorMsg } = useFormFieldValidation({ name: usedField, value: value });
	return (
		<div data-testid={'component'}>
			<span data-testid={'isValid'}>{ isValid ? 'true' : 'false' }</span>
			<span data-testid={'errorMsg'}>{ errorMsg }</span>
		</div>
	);
}

describe('useFormFieldValidation', () => {
	it('Renders the component as Valid when a value is present', async () => {
		const { queryByTestId } = render(<TestComponent value={'value'} />);
		await waitFor(() => {
			expect(queryByTestId('isValid')).toHaveTextContent('true');
			expect(queryByTestId('errorMsg')).toHaveTextContent('');
		});
	});

	it('Renders the component as Valid, when value is: undefined', async () => {
		const { queryByTestId } = render(<TestComponent value={undefined} />);
		await waitFor(() => {
			expect(queryByTestId('isValid')).toHaveTextContent('true');
			expect(queryByTestId('errorMsg')).toHaveTextContent('');
		});
	});

	it('Renders the component as in-valid, when value is: an empty string', async () => {
		const { queryByTestId } = render(<TestComponent value={''} />);
		await waitFor(() => {
			expect(queryByTestId('isValid')).toHaveTextContent('false');
			expect(queryByTestId('errorMsg')).toHaveTextContent(`${usedField} is a required field`);
		});
	});
});