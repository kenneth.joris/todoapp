"use client";

import { useEffect, useState } from 'react';
import { fieldValidationTypes, validationErrors } from '@/00_global/utils/validation';
import { object } from 'yup';

type UseValidationProps = {
	name: string;
	value: string | undefined;
}

export const useFormFieldValidation = ({ name, value }: UseValidationProps) => {
	const [isValid, setIsValid] = useState<boolean>(true);
	const [errorMsg, setErrorMsg] = useState<string>('');

	useEffect(() => {
		const fieldValidation = fieldValidationTypes[name];
		if (!fieldValidation) return;

		// Validation Schema
		const schema = object().shape({ [name]: fieldValidation });

		// isValid
		fieldValidation
			.isValid(value)
			.then((isDataValid: boolean) => setIsValid(isDataValid));

		// errorMsg
		validationErrors(schema, { [name]: value })
			.then((errorMessages) => {
				if (!!errorMessages) {
					const errorMessage = (errorMessages as { [key: string]: string[]; })?.[name]?.[0];
					setErrorMsg(errorMessage);
				}
			});

	}, [value, name]);

	return {
		isValid: isValid,
		errorMsg: errorMsg,
	};
}