"use client";

// Bases on source, but altered - https://github.com/imbhargav5/rooks/blob/main/src/hooks/useSessionstorage.ts
// const { sessionStoreValue, setSessionStoreValue, removeSessionStoreValue } = useSessionStorage('storage-key', defaultValue);


import { useEffect, useReducer, useCallback } from 'react';

const reducer = (state: unknown | null, action: { type: string, payload: unknown }) => {
	switch (action.type) {
		case 'set':
			return action.payload;
		default:
			return state;
	}
};

export const useSessionStorage = (key: string, defaultValue: unknown | null = null) => {
	const [sessionStoreValue, dispatch] = useReducer(
		reducer,
		getValueFromSessionStorage()
	);

	const init = () => {
		const initialValue = getValueFromSessionStorage();
		if (initialValue === null || initialValue === 'null') {
			setSessionStoreValue(defaultValue);
		}
	}

	function getValueFromSessionStorage() {
		if (typeof sessionStorage === 'undefined') return null;
		const storedValue = sessionStorage.getItem(key) || 'null';
		try {
			return JSON.parse(storedValue);
		} catch (error) {
			console.error(error);
		}

		return storedValue;
	}

	const saveValueToSessionStorage = (valueToSet: unknown | null) => {
		if (typeof sessionStorage === 'undefined') return null;
		return sessionStorage.setItem(key, JSON.stringify(valueToSet));
	}

	const setValue = (valueToSet: unknown | null) => {
		dispatch({ payload: valueToSet, type: 'set' });
	}

	const setSessionStoreValue = (newValue: unknown | null) => {
		saveValueToSessionStorage(newValue);
		setValue(newValue);
	}

	const removeSessionStoreValue = () => {
		if (typeof sessionStorage === 'undefined') return null;
		sessionStorage.removeItem(key);
		setValue(null);
	}

	useEffect(() => {
		void init();

	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const listen = useCallback((event: StorageEvent) => {
		if (event.storageArea === sessionStorage && event.key === key) {
			setSessionStoreValue(event.newValue);
		}

	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	useEffect(() => {
		window.addEventListener('storage', listen);
		return () => {
			window.removeEventListener('storage', listen);
		};

	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	return {
		sessionStoreValue,
		setSessionStoreValue,
		removeSessionStoreValue
	};
}
