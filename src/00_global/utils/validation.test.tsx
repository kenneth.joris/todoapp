import { object } from 'yup';
import { FIELDNAMES } from '@/00_global/constants';
import { fieldValidationTypes, isFormValid } from '@/00_global/utils/validation';


describe('validation', () => {
	it('validates data as valid when value is: defined', async () => {
		const fieldValidation = fieldValidationTypes[FIELDNAMES.NAME];
		const schema = object().shape({ [FIELDNAMES.NAME]: fieldValidation });
		const validationResult = await isFormValid(schema, { [FIELDNAMES.NAME]: 'value' });
		expect(validationResult).toBe(true);
	});

	it('validates data as invalid when value is: an empty string', async () => {
		const fieldValidation = fieldValidationTypes[FIELDNAMES.NAME];
		const schema = object().shape({ [FIELDNAMES.NAME]: fieldValidation });
		const validationResult = await isFormValid(schema, { [FIELDNAMES.NAME]: '' });
		expect(validationResult).toBe(false);
	});

	it('validates data as valid when value is: undefined', async () => {
		const fieldValidation = fieldValidationTypes[FIELDNAMES.NAME];
		const schema = object().shape({ [FIELDNAMES.NAME]: fieldValidation });
		const validationResult = await isFormValid(schema, { [FIELDNAMES.NAME]: undefined });
		expect(validationResult).toBe(false);
	});
});