import { string, Schema, ValidationError } from 'yup';
import { FIELDNAMES } from '@/00_global/constants';

// Validation variables
const nameRegex = /^[A-Za-z\u00C0-\u00FF ,.'-]*$/;

export const fieldValidationTypes: { [key: string]:  Schema<any, any, any, "">} = {
	// Login
	[FIELDNAMES.NAME]: string().matches(nameRegex).trim().required(),
	[FIELDNAMES.EMAIL]: string().trim().email().required(),
	[FIELDNAMES.PASSWORD]: string().trim().required(),

	// Todos
	[FIELDNAMES.TITLE]: string().trim().required(),
	[FIELDNAMES.DESCRIPTION]: string().trim().required(),
	[FIELDNAMES.ASSIGNEE]: string().trim().required(),
};

export const isFormValid = async (formSchema: Schema, formData: unknown): Promise<boolean> => {
	return await formSchema.isValid(formData).then((validationResult: boolean) => validationResult);
};

export const validationErrors = (formSchema: Schema, formData: unknown) => {
	return (
		formSchema
			.validate(formData, { abortEarly: false })
			// form valid return no error messages
			.then(() => {
				return false;
			})
			// form has error messages
			.catch(function (validationError) {
				const errorsCollection:  ValidationError[] = validationError.inner;
				let fieldErrorCollection: { [key: string]: string[] } = {};

				errorsCollection.forEach((item: ValidationError) => {
					if (item.value !== undefined && item.path !== undefined) {
						fieldErrorCollection[item.path] = item?.errors;
					}
				});

				return !!fieldErrorCollection && fieldErrorCollection;
			})
	);
};


