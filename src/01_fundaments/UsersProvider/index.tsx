import { ReactNode } from 'react';
import { UsersContextProvider } from '@/00_global/context/useUsers';

type NextAuthProviderProps = {
	children: ReactNode;
}

export default function UsersProvider({
	children,
}: NextAuthProviderProps) {
	return <UsersContextProvider>{children}</UsersContextProvider>;
}