import Button, { ButtonProps } from '@/02_atoms/Button/index';
import "@testing-library/jest-dom";
import { fireEvent, render, screen } from "@testing-library/react";

const props = {
	type: 'button',
	onClick: jest.fn(),
	children: 'Button',
} as ButtonProps;


describe("Button", () => {
	it("renders as Button", () => {
		render(<Button {...props} />);
		const button = screen.getByRole('button');
		expect(button).toBeInTheDocument();
	});

	it('Renders as anchor tag when href is present', () => {
		render(<Button {...props} href={'/'} />);
		const anchor = screen.getByRole('link');
		expect(anchor).toBeInTheDocument();
	});

	it('Calls onClick when clicked', () => {
		render(<Button {...props} />);
		const button = screen.getByRole('button');
		fireEvent.click(button);
		expect(props.onClick).toHaveBeenCalledTimes(1);
	});

	it('Does not call onClick when disabled', () => {
		render(<Button {...props} disabled={true} />);
		const button = screen.getByRole('button');
		fireEvent.click(button);
		expect(props.onClick).toHaveBeenCalledTimes(0);
	});

	it('Does not call onClick when href is present', () => {
		render(<Button {...props} href={'/'} />);
		const anchor = screen.getByRole('link');
		fireEvent.click(anchor);
		expect(props.onClick).toHaveBeenCalledTimes(0);
	});
});