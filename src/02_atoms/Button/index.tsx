"use client";

import React from 'react';
import Link from 'next/link';

export type ButtonProps = {
	type?: 'button' | 'submit';
	onClick?: () => void;
	color?: 'indigo' | 'blue' | 'white';
	href?: string;
	children: React.ReactNode;
	classNames?: string;
	disabled?: boolean;
};

const colorClassNames = {
	indigo: 'bg-indigo-600 hover:bg-indigo-500 focus-visible:outline-indigo-600 text-white',
	blue: 'bg-blue-600 hover:bg-blue-500 focus-visible:outline-blue-600 text-white',
	white: 'bg-white-600 hover:bg-white-500 focus-visible:outline-white-600 text-black ring-1 ring-inset ring-gray-300',
};

const Button = ({
	type = 'button',
	onClick,
	color = 'indigo',
	href,
	classNames,
	disabled,
	children
}: ButtonProps) => {
	if (!!href) {
		return (
			<Link
				href={href}
				className={`${classNames} ${colorClassNames[color]} rounded-md px-3.5 py-2.5 text-sm font-semibold shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2`}
			>{ children }</Link>
		)
	}

	return (
		<button
			type={type}
			className={`${classNames} ${colorClassNames[color]} ${disabled && 'opacity-30'} rounded-md px-3.5 py-2.5 text-sm font-semibold shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2`}
			onClick={() => onClick?.()}
			disabled={disabled}
		>{ children }</button>
	)
};

export default Button;