import "@testing-library/jest-dom";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import Input, { InputProps } from '@/02_atoms/Input/index';

const props = {
	type: 'text',
	name: 'name',
	placeholder: 'placeholder',
	value: 'value',
	onChange: jest.fn(),
} as InputProps;


describe("Input", () => {
	it("renders as Input", () => {
		render(<Input {...props} />);
		const input = screen.getByRole('textbox');
		expect(input).toBeInTheDocument();
	});

	it('Sets the correct type', async () => {
		const { container } = render(<Input {...props} />);
		await waitFor(() => {
			expect(container.querySelector(`[type=${props.type}]`)).toBeInTheDocument();
		});
	});

	it('Sets the correct name & id', async () => {
		const { container } = render(<Input {...props} />);
		await waitFor(() => {
			expect(container.querySelector(`[name=${props.name}]`)).toBeInTheDocument();
			expect(container.querySelector(`[id=${props.name}]`)).toBeInTheDocument();
		});
	});

	it('Calls onChange', () => {
		render(<Input {...props} />);
		const input = screen.getByRole('textbox');
		fireEvent.change(input, { target: { value: "New Todo" } });
		expect(props.onChange).toHaveBeenCalledTimes(1);
	});

	it('Shows the placeholder', async () => {
		const { getByPlaceholderText } = render(<Input {...props} value={''} />);
		await waitFor(() => {
			expect(getByPlaceholderText(`${props.placeholder}`)).toBeInTheDocument();
		});
	});

	it('Shows an error when invalid', async () => {
		const { getByText } = render(<Input {...props} value={''} />);
		await waitFor(() => {
			expect(getByText(`${props.name} is a required field`)).toBeInTheDocument();
		});
	});
});