import { ChangeEvent } from 'react';
import { useFormFieldValidation } from '@/00_global/hooks/useFormFieldValidation';

type OptionsType = {
	key: string;
	value: string;
}

type SelectProps = {
	name: string;
	placeholder?: string;
	value: string;
	onChange: (e: ChangeEvent<HTMLSelectElement>) => void;
	options: OptionsType[];
}

const Select = ({
	name,
	placeholder,
	value = '',
	onChange,
	options = [],
}: SelectProps) => {

	const { isValid, errorMsg} = useFormFieldValidation({ name: name, value: value});

	return (
		<>
			<select
				name={name}
				id={name}
				className="mt-2 block w-full rounded-md border-0 p-3 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
				placeholder={placeholder}
				value={value}
				onChange={onChange}
			>
				<option value={''} disabled={true}>Select an option</option>
				{
					options.map(({ key, value }) => (
						<option key={value} value={value}>{key}</option>
					))
				}
			</select>
			{
				!isValid && (
					<p className={'text-red-500 mt-2 text-xs'}>{ errorMsg }</p>
				)
			}
		</>
	);
};

export default Select;