"use client";

import React from 'react';
import { signIn, signOut, useSession } from 'next-auth/react';
import Button from '@/02_atoms/Button';
import { FIELDNAMES } from '@/00_global/constants';

const SignInOutButton = () => {
	const { data: session } = useSession();
	const loggedIn = !!session?.user?.[FIELDNAMES.NAME];

	const handleAuthButtonClick = () => {
		if (loggedIn) return signOut();
		return signIn();
	};

	return (
		<Button onClick={handleAuthButtonClick}>Sign { loggedIn ? 'out' : 'in'}</Button>
	);
};

export default SignInOutButton;