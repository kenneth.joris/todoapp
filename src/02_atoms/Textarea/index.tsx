import React, { ChangeEvent } from 'react';
import { useFormFieldValidation } from '@/00_global/hooks/useFormFieldValidation';

type TextareaProps = {
	name: string;
	placeholder?: string;
	value: string | undefined;
	onChange: (e: ChangeEvent<HTMLTextAreaElement>) => void;
	rows?: number;
}

const Textarea = ({
	name,
	placeholder,
	value,
	onChange,
	rows = 4,
}: TextareaProps) => {

	const { isValid, errorMsg} = useFormFieldValidation({ name: name, value: value});

	return (
		<>
			<textarea
				rows={rows}
				name={name}
				id={name}
				className="block w-full rounded-md border-0 p-3 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
				placeholder={placeholder}
				value={value}
				onChange={onChange}
			></textarea>
			{
				!isValid && (
					<p className={'text-red-500 mt-2 text-xs'}>{ errorMsg }</p>
				)
			}
		</>
	);
};

export default Textarea;