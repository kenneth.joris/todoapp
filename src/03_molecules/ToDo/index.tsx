import React, { useState } from 'react';
import TodoEditor from '@/03_molecules/TodoEditor';
import TodoPreview from '@/03_molecules/TodoPreview';
import { TodoType } from '@/00_global/context/useTodos';

const ToDo = ({
	id,
	title,
	description,
	assignee,
	done,
	editMode
}: TodoType) => {

	if (editMode) return (
		<TodoEditor
			id={id}
			title={title}
			description={description}
			assignee={assignee}
			done={done}
			editMode={editMode}
		/>
	);
	return (
		<TodoPreview
			id={id}
			title={title}
			description={description}
			assignee={assignee}
			done={done}
		/>
	);
};

export default ToDo;