"use client";

import { useTodos } from '@/00_global/context/useTodos';
import Button from '@/02_atoms/Button';

const TodoAdd = () => {
	const { addTodo } = useTodos();
	return (
		<li>
			<div className="flex min-w-0 gap-x-4 rounded-sm bg-white shadow p-5">
				<div className="ml-auto mr-auto">
					<Button onClick={addTodo} type={'button'}>+ Add Todo</Button>
				</div>
			</div>
		</li>
	);
};

export default TodoAdd;