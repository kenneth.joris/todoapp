"use client";

import { ChangeEvent, useEffect, useRef, useState } from 'react';
import { TodoType, useTodos } from '@/00_global/context/useTodos';
import { useUsers } from '@/00_global/context/useUsers';
import Image from 'next/image';
import Input from '@/02_atoms/Input';
import Label from '@/02_atoms/Label';
import { FIELDNAMES } from '@/00_global/constants';
import Textarea from '@/02_atoms/Textarea';
import Select from '@/02_atoms/Select';
import Button from '@/02_atoms/Button';
import { User } from '@/00_global/context/useUsers';
import { fieldValidationTypes, isFormValid } from '@/00_global/utils/validation';
import { object } from 'yup';

const validationSchema = object().shape({
	[FIELDNAMES.TITLE]: fieldValidationTypes[FIELDNAMES.TITLE],
	[FIELDNAMES.DESCRIPTION]: fieldValidationTypes[FIELDNAMES.DESCRIPTION],
	[FIELDNAMES.ASSIGNEE]: fieldValidationTypes[FIELDNAMES.ASSIGNEE],
});

const TodoEditor = ({
	id,
	title,
	description,
	assignee,
	done,
	editMode
}: TodoType) => {

	// Contexts
	const { updateTodo, deleteTodo} = useTodos();
	const { users } = useUsers();

	// Form data
	const initialFormData = useRef({
		id,
		title,
		description,
		assignee,
		done,
		editMode
	})
	const [formData, setFormData] = useState<TodoType>(initialFormData.current);

	// Validation
	const [formValid, setFormValid] = useState(true);
	useEffect(() => {
		const validationResult = isFormValid(validationSchema, formData);
		validationResult.then((isValid) => setFormValid(isValid));

	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [formData.title, formData.description, formData.assignee]);


	// Assignee
	const getAssignedUser = () => users.find((user) => user.login.username === formData.assignee);
	const [assignedUserData, setAssignedUserData] = useState<User | undefined>(getAssignedUser())
	useEffect(() => {
		setAssignedUserData(getAssignedUser());

	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [formData.assignee]);

	// Methods
	const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => {
		const name = e.target.name;
		const value = e.target.value;
		setFormData({ ...formData, [name]: value });
	};
	const handleClose = () => updateTodo({ id: id, editMode: false });
	const handleSave = () => updateTodo({ ...formData, editMode: false });
	const handleCancel = () => setFormData(initialFormData.current);
	const handleDelete = () => deleteTodo({ id: id });

	return (
		<li>
			<form
				className="flex min-w-0 gap-x-4 rounded-sm bg-white shadow p-5"
				autoComplete={'off'}
				noValidate={true}
			>
				<Image
					className="h-12 w-12 flex-none rounded-full bg-gray-50"
					src={assignedUserData?.picture?.thumbnail ?? '/tumbnail-fallback.jpg'}
					alt={assignedUserData?.name?.first ?? 'tumbnail'}
					width={48}
					height={48}
				/>
				<div className="w-full">
					{/* title */}
					<div>
						<Label htmlFor={FIELDNAMES.TITLE}>Title</Label>
						<div className="mt-2">
							<Input
								type={'text'}
								name={FIELDNAMES.TITLE}
								placeholder={'Title'}
								value={formData.title}
								onChange={handleChange}
							/>
						</div>
					</div>
					{/* description */}
					<div className="mt-3">
						<Label htmlFor={FIELDNAMES.DESCRIPTION}>Description</Label>
						<div className="mt-2">
							<Textarea
								name={FIELDNAMES.DESCRIPTION}
								placeholder={'Description'}
								value={formData.description}
								onChange={handleChange}
							/>
						</div>
					</div>
					{/* assignee */}
					<div className="mt-3">
						<Label htmlFor={FIELDNAMES.ASSIGNEE}>Assignee</Label>
						<Select
							name={FIELDNAMES.ASSIGNEE}
							value={formData.assignee}
							onChange={handleChange}
							options={users.map((user) => ({
								key: user.name.first,
								value: user.login.username,
							}))}
						/>
					</div>
				</div>
				<div className="ml-auto flex flex-col">
					{/* actions */}
					<Button
						type={'button'}
						onClick={handleClose}
						color={'blue'}
						classNames={'ml-auto'}
					>X</Button>
					<Button
						type={'button'}
						onClick={handleSave}
						color={'white'}
						classNames={'w-full mt-3'}
						disabled={!formValid}
					>Save</Button>
					<Button
						type={'button'}
						onClick={handleCancel}
						color={'white'}
						classNames={'mt-3 w-full'}
					>Cancel</Button>
					<Button
						type={'button'}
						onClick={handleDelete}
						color={'white'}
						classNames={'mt-3 w-full'}
					>Delete</Button>
				</div>
			</form>
		</li>
	);
};

export default TodoEditor;