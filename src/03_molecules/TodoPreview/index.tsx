"use client";

import React from 'react';
import { TodoType, useTodos } from '@/00_global/context/useTodos';
import Button from '@/02_atoms/Button';
import Image from 'next/image';
import { useUsers } from '@/00_global/context/useUsers';

const TodoPreview = ({
	id,
	title,
	description,
	assignee,
	done,
}: Omit<TodoType, 'editMode'>) => {

	const { users } = useUsers();
	const { updateTodo, deleteTodo } = useTodos();

	const assignedUserData = users.find((user) => user.login.username === assignee);

	const handleDone = () => updateTodo({ id: id, done: true, editMode: false });
	const handleEdit = () => updateTodo({ id: id, editMode: true });
	const handleDelete = () => deleteTodo({ id: id });

	return (
		<li>
			<div className="md:flex min-w-0 gap-x-4 rounded-sm bg-white shadow p-5">
				<Image
					className="h-12 w-12 flex-none rounded-full bg-gray-50"
					src={assignedUserData?.picture?.thumbnail ?? '/tumbnail-fallback.jpg'}
					alt={assignedUserData?.name?.first ?? 'tumbnail'}
					width={48}
					height={48}
				/>
				<div className="mt-3 md:mt-0">
					<p className="text-sm font-semibold leading-6 text-gray-900">{ title }</p>
					<p className="mt-1 text-xs leading-5 text-gray-500">{ description }</p>
				</div>
				<div className="ml-auto mt-3 md:mt-0">
					<span className="isolate inline-flex rounded-md">
						{
							!done && (
								<>
									<Button
										type={'button'}
										onClick={handleDone}
										color={'white'}
										classNames={'ml-3'}
									>Done</Button>
									<Button
										type={'button'}
										onClick={handleEdit}
										color={'white'}
										classNames={'ml-3'}
									>Edit</Button>
								</>
							)
						}
						{
							done && (
								<Button
									type={'button'}
									onClick={handleDelete}
									color={'white'}
									classNames={'ml-3'}
								>Delete</Button>
							)
						}
					</span>
				</div>
			</div>
		</li>
	);
};

export default TodoPreview;