"use client";

import TodoPreview from '@/03_molecules/TodoPreview';
import { useTodos } from '@/00_global/context/useTodos';

const FinishedTodos = () => {
	const { todos } = useTodos();

	return (
		<div className="gap-4 col-span-1 flex flex-col">
			<h2 className="text-center text-lg font-semibold leading-8">DONE</h2>
			<ul className="gap-4 col-span-1 flex flex-col">
				{
					todos
						.filter((todo) => todo.done)
						.map(({
							id,
							title,
							description,
							assignee,
							done,
						}) => (
							<TodoPreview
								key={id}
								id={id}
								title={title}
								description={description}
								assignee={assignee}
								done={done}
							/>
						))
				}
			</ul>
		</div>
	);
};

export default FinishedTodos;