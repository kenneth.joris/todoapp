"use client";

import SignInOutButton from '@/02_atoms/SignInOutButton';
import Button from '@/02_atoms/Button';
import { PATHS } from '@/00_global/constants';
import { usePathname } from 'next/navigation';
import { useUser } from '@/00_global/context/useUser';
import Image from 'next/image';
import Link from 'next/link';

const MainNavigation = () => {
	const pathname = usePathname();
	const { image, name } = useUser();

	return (
		<nav className="bg-white shadow">
			<div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
				<div className="flex h-16 justify-between">
					<div className="flex">
						{/*	Logo */}
						<div className="flex flex-shrink-0 items-center">
							<Link href={PATHS.HOME}>
								<Image
									src={'/next.svg'}
									alt={'Todo app logo'}
									width={60}
									height={60}
								/>
							</Link>
						</div>

						{/*	Nav items*/}
						<div className="ml-6 flex items-center space-x-4">
							<Button href={PATHS.TODOS} color={PATHS.TODOS === pathname ? 'blue' : 'white'}>Todos</Button>
						</div>
					</div>
					<div className="ml-6 flex items-center">
						<SignInOutButton />
						{
							image && (
								<div className="ml-6 flex flex-shrink-0 items-center">
									<Image
										className="inline-block h-12 w-12 rounded-full"
										src={image}
										alt={name}
										width={48}
										height={48}
									/>
								</div>
							)
						}
					</div>
				</div>
			</div>
		</nav>
	);
};

export default MainNavigation;