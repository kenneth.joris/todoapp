"use client";

import TodoAdd from '@/03_molecules/TodoAdd';
import { useTodos } from '@/00_global/context/useTodos';
import ToDo from '@/03_molecules/ToDo';

const UnfinishedTodos = () => {
	const { todos } = useTodos();

	return (
		<div className="gap-4 col-span-1 flex flex-col">
			<h2 className="text-center text-lg font-semibold leading-8">TODO</h2>
			<ul className="gap-4 col-span-1 flex flex-col">
				{
					todos
					.filter((todo) => !todo.done)
					.map(({
						id,
						title,
						description,
						assignee,
						done,
						editMode
					}) => (
						<ToDo
							key={id}
							id={id}
							title={title}
							description={description}
							assignee={assignee}
							done={done}
							editMode={editMode}
						/>
					))
				}

				{/* Add */}
				<TodoAdd />

			</ul>
		</div>
	);
};

export default UnfinishedTodos;