import Button from '@/02_atoms/Button';
import { FIELDNAMES, PATHS } from '@/00_global/constants';
import SignInOutButton from '@/02_atoms/SignInOutButton';
import { useSession } from 'next-auth/react';

const Greeting = ({ userName }: { userName: string | undefined }) => (
	<h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
		{
			!!userName
				? (<>Welcome back <span className="text-blue-500 capitalize">{userName}</span>.<br />Let&apos;s boost your productivity.</>)
				: (<>Boost your productivity.<br />Start using our app today.</>)
		}
	</h2>
);

const HomePage = () => {

	const { data: session } = useSession();
	const userName = session?.user?.[FIELDNAMES.NAME] ?? '';

	return (
		<div className="bg-white">
			<div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8 py-24 sm:py-32">
				<div className="mx-auto max-w-2xl text-center">
					<Greeting userName={userName} />
					<div className="mt-10 flex items-center justify-center gap-x-6">
						{
							!!userName && (
								<Button href={PATHS.TODOS} color={'blue'}>View TODO&apos;s</Button>
							)
						}
						<SignInOutButton />
					</div>
				</div>
			</div>
		</div>
	)
};

export default HomePage;