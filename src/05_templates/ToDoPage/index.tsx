import UnfinishedTodos from '@/04_organisms/UnfinishedTodos';
import FinishedTodos from '@/04_organisms/FinishedTodos';

const ToDoPage = () => {
	return (
		<div className="bg-white">
			<div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8 py-24 sm:py-32">
				<div className="mx-auto">
					<div className="grid grid-cols-1 gap-4 lg:grid-cols-2">
						{/* TODOS */}
						<UnfinishedTodos />

						{/* DONE */}
						<FinishedTodos />
					</div>
				</div>
			</div>
		</div>
	)
}

export default ToDoPage;