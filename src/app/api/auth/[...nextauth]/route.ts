import NextAuth, { AuthOptions } from 'next-auth';
import CredentialsProvider from "next-auth/providers/credentials";
import { API_ENDPOINTS, FIELDNAMES } from '@/00_global/constants';
import { object } from "yup";
import { fieldValidationTypes, isFormValid } from '@/00_global/utils/validation';

const authOptions: AuthOptions = {
	secret: process.env.NEXTAUTH_SECRET as string,
	providers: [
		CredentialsProvider({
			name: "Credentials",
			credentials: {
				[FIELDNAMES.NAME]: {
					label: "Username",
					type: "text",
					placeholder: "Will always be: Amy"
				},
				[FIELDNAMES.EMAIL]: {
					label: "Email",
					type: "text",
					placeholder: "Any email will do"
				},
				[FIELDNAMES.PASSWORD]: {
					label: "Password",
					type: "password",
					placeholder: "Any string will do"
				}
			},
			async authorize(credentials, req) {

				// If data is missing - disallow authorization
				if (
					!credentials?.[FIELDNAMES.NAME] ||
					!credentials?.[FIELDNAMES.EMAIL] ||
					!credentials?.[FIELDNAMES.PASSWORD]
				) { return null; }

				// Validation schema
				const userSchema = object().shape({
					[FIELDNAMES.NAME]: fieldValidationTypes[FIELDNAMES.NAME],
					[FIELDNAMES.EMAIL]: fieldValidationTypes[FIELDNAMES.EMAIL],
					[FIELDNAMES.PASSWORD]: fieldValidationTypes[FIELDNAMES.PASSWORD],
				});

				// Define data for validation
				const userData = {
					[FIELDNAMES.NAME]: credentials[FIELDNAMES.NAME],
					[FIELDNAMES.EMAIL]: credentials[FIELDNAMES.EMAIL],
					[FIELDNAMES.PASSWORD]: credentials[FIELDNAMES.PASSWORD],
				};

				// Validate it.
				try {
					const isValid = await isFormValid(userSchema, userData);
					if (isValid) {
						// TODO: Add logic here to look up the user from the supplied credentials.
						return {
							[FIELDNAMES.ID]: "1",
							[FIELDNAMES.NAME]: credentials[FIELDNAMES.NAME],
							[FIELDNAMES.EMAIL]: credentials[FIELDNAMES.EMAIL],
						};
					}

					// not Valid - disallow authorization
					return null;

				} catch (e) {
					// TODO: Present an UI to notify the user.
					console.error(e);
					return null;
				}
			}
		})
	],
	callbacks: {

		// TODO: add typings for Session
		// @ts-ignore
		async session({ session } ) {
			await fetch(API_ENDPOINTS.RANDOM_USER, {
				cache: 'force-cache',
				method: 'GET',
				headers: { "Content-Type": "application/json" },
			}).then(async (resp) => {
				if (!resp.ok) {
					throw new Error('Failed to fetch user!');
				}
				return await resp.json();
			}).then(({ results }) => {
				if (!results?.length) return;

				const { name, login, picture } = results[0];
				session.user.name = name?.first;
				session.user.id = login?.username;
				session.user.image = picture?.thumbnail;

			}).catch((e) => console.error(e));

			return session
		}
	}
};
const handler = NextAuth(authOptions);
export { handler as GET, handler as POST }