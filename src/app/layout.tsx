import '@/00_global/styles/index.scss';
import type { Metadata } from 'next'
import { Inter } from 'next/font/google';
import NextAuthProvider from '@/01_fundaments/NextAuthProvider';
import MainNavigation from '@/04_organisms/MainNavigation';
import { UserContextProvider } from '@/00_global/context/useUser';

type RootLayoutProps = {
  children: React.ReactNode
};

const inter = Inter({
    subsets: ['latin'],
    weight: ['300', '600'],
    display: 'swap',
    variable: '--font-family',
});

export const metadata: Metadata = {
  title: 'ToDo App',
  description: 'A Simple todo app with auth',
  viewport: {
    width: 'device-width',
    initialScale: 1,
    minimumScale: 1,
    maximumScale: 1,
  },
}

export default function RootLayout({ children }: RootLayoutProps) {
    return (
        <html lang="en" className={`${inter.variable}`}>
            <head>
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            </head>
            <body>
                <NextAuthProvider>
                    <UserContextProvider>
                        <MainNavigation />
                        {children}
                    </UserContextProvider>
                </NextAuthProvider>
            </body>
        </html>
    )
}
