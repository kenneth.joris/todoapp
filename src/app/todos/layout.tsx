import React from 'react';
import UsersProvider from '@/01_fundaments/UsersProvider';
import { TodosContextProvider } from '@/00_global/context/useTodos';

type LayoutProps = {
	children: React.ReactNode
};

const Layout = ({ children }: LayoutProps) => {
	return (
		<UsersProvider>
			<TodosContextProvider>
				{ children }
			</TodosContextProvider>
		</UsersProvider>
	);
};

export default Layout;