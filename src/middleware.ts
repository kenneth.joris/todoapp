export { default } from "next-auth/middleware";

// Authorization is required for all todos.
export const config = {
	matcher: ["/todos", "/todos/:path*"],
};

